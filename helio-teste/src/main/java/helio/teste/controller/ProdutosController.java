package helio.teste.controller;

import static br.com.caelum.vraptor.view.Results.json;
import helio.teste.model.Produto;
import helio.teste.model.ProdutoDAO;

import java.util.List;

import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;

@Resource
// para controller vRaptor
public class ProdutosController {

	private final ProdutoDAO dao;
	private final Result result;
	private final Validator validator;

	public ProdutosController(ProdutoDAO dao, Result result, Validator validator) {
		this.dao = dao;
		this.result = result;
		this.validator = validator;
	}

	@Get("/produtos")
	public List<Produto> lista() {
		return dao.listTudo();
	}
	@Restrito
	@Post("/produtos")
	public void adiciona(Produto produto) {
		valida(produto);
		validator.onErrorUsePageOf(ProdutosController.class).formulario();
		dao.cadastrar(produto);
		result.redirectTo(this).lista();
	}

	@Get("/produtos/menu")
	public void menu() {
	}
	@Restrito
	@Get("/produtos/novo")
	public void formulario() {
	}
	@Restrito
	@Get("/produtos/{id}")
	public Produto edita(int id) {
		return dao.consulta(id);

	}
	@Restrito
	@Put("/produtos/{produto.id}")
	public void altera(Produto produto) {
		valida(produto);
		validator.onErrorUsePageOf(ProdutosController.class).edita(
				produto.getId());
		dao.alterar(produto);
		result.redirectTo(this).lista();
	}
	@Restrito
	@Delete("/produtos/{id}")
	public void remove(int id) {
		Produto produto = dao.consulta(id);
		dao.excluir(produto);
		result.redirectTo(this).lista();
	}

	public void valida(Produto produto) {
		if (produto.getNome() == null || produto.getNome().length() < 3) {
			validator.add(new ValidationMessage(
					"Nome é obrigatório e precisa ter mais" + " de 3 letras",
					"produto.nome"));
		}
		if (produto.getDescricao() == null
				|| produto.getDescricao().length() > 40) {
			validator.add(new ValidationMessage(
					"Descrição é obrigatória não pode ter mais"
							+ " que 40 letras", "produto.descricao"));
		}
		if (produto.getPreco() <= 0) {
			validator.add(new ValidationMessage("Preço precisa ser positivo",
					"produto.preco"));
		}

	}

	@Get("/produto/busca")
	public List<Produto> busca(String nome) {
		result.include("nome", nome);
		return dao.busca(nome);
	}

	@Get("/produtos/busca.json")
	public void buscaJson(String q) {
		result.use(json()).withoutRoot().from(dao.busca(q))
				.exclude("id", "descricao").serialize();
	}

}
