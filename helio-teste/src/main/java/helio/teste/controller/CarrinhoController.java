package helio.teste.controller;

import helio.teste.model.Carrinho;
import helio.teste.model.Item;
import helio.teste.model.ProdutoDAO;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;

@Resource
public class CarrinhoController {
	private final Carrinho carrinho;
	private final ProdutoDAO dao;
	private final Result result;

	public CarrinhoController(Carrinho carrinho, ProdutoDAO dao, Result result) {
		this.carrinho = carrinho;
		this.dao = dao;
		this.result = result;
	}

	@Post("/carrinho")
	public void adiciona(Item item) {
		dao.recarrega(item.getProduto());
		carrinho.adiciona(item);
		result.redirectTo(this).visualiza();
	}

	@Delete("/carrinho/{indiceItem}")
	public void remove(int indiceItem) {
		carrinho.remove(indiceItem);
		result.redirectTo(this).visualiza();
	}

	@Get("/carrinho")
	public void visualiza() {
	}
}
