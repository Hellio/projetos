package helio.teste.model;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
// hibernate transação para acesso ao banco
public class ProdutoDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public ProdutoDAO(@Qualifier("sessionFactory") SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public void cadastrar(Produto produto) {
		getSession().merge(produto);
	}

	public void excluir(Produto produto) {
		getSession().delete(produto);
	}

	public void alterar(Produto produto) {
		getSession().update(produto);
	}

	public Produto consulta(int id) {
		return (Produto) getSession().get(Produto.class, id);

	}

	@SuppressWarnings("unchecked")
	public List<Produto> listTudo() {
		return getSession().createCriteria(Produto.class).list();
	}

	public List<Produto> busca(String nome) {
		return getSession().createCriteria(Produto.class).add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE)).list();
	
	}
	public void recarrega(Produto produto) {
		 getSession().refresh(produto);
		}

}
