<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3><a href="<c:url value="/produtos/menu"/>">Menu</a></h3>
<div id="erros">
	<ul>
		<c:forEach items="${errors}" var="error">
			<li>${error.message }</li>
		</c:forEach>
	</ul>
</div>
<form action="<c:url value="/produtos/${produto.id }"/>" method="POST">
	<fieldset>
		<legend>Editar Produto</legend>
		<label for="nome">Nome:</label> <input id="nome" type="text"
			name="produto.nome" value="${produto.nome }" /> <label
			for="descricao">Descri��o:</label>
		<textarea id="descricao" name="produto.descricao">
		${produto.descricao }
		</textarea>
		<label for="preco">Pre�o:</label> <input id="preco" type="text"
			name="produto.preco" value="${produto.preco }" />
		<button type="submit" name="_method" value="PUT">Enviar</button>
	</fieldset>
</form>
<form action="<c:url value="/produtos/${produto.id }/imagem"/>"
	method="POST" enctype="multipart/form-data">
	<fieldset>
		<legend>Upload de Imagem</legend>
		<input type="file" name="imagem" />
		<button type="submit">Enviar</button>
	</fieldset>
</form>