<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script type="text/javascript"
	src="<c:url value="/js/jquery-2.1.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery-ui.min.js"/>"></script>
<link href="<c:url value="/css/jquery-ui.min.css"/>" rel="stylesheet"
	type="text/css" media="screen" />
<link href="<c:url value="/css/jquery-ui.structure.min.css"/>"
	rel="stylesheet" type="text/css" media="screen" />
<link href="<c:url value="/css/jquery-ui.theme.min.css"/>"
	rel="stylesheet" type="text/css" media="screen" />

<!--<script type="text/javascript" src="<c:url value="/css/jquery.autocomplete.css"/>"></script> 
 <script type="text/javascript" src="<c:url value="/js/jquery.puts.js"/>"></script>-->

<div id="menu">
	<div id="usuario">
		<c:if test="${usuarioWeb.logado}">
Ol�, ${usuarioWeb.nome }!
<a href="<c:url value="/logout"/>">Logout</a>
		</c:if>
		<c:if test="${empty usuarioWeb or not usuarioWeb.logado}">Voc� n�o est� logado.
<a href="<c:url value="/usuario/login"/>">Login</a>
			<a href="<c:url value="/usuario/novo"/>">Cadastre-se</a>
		</c:if>
	</div>
	<div id="carrinho">
		<h3>
			<a href="<c:url value="/carrinho"/>">Meu carrinho:</a>
		</h3>
		<c:if test="${empty carrinho or carrinho.totalDeItens eq 0 }">
			<span>Voc� n�o possui itens no seu carrinho</span>
		</c:if>
		<c:if test="${carrinho.totalDeItens > 0 }">
			<ul>
				<li><strong>Itens:</strong> ${carrinho.totalDeItens }</li>
				<li><strong>Total:</strong>${carrinho.total }</li>
			</ul>
		</c:if>
	</div>
	<ul>
		<c:if test="${usuarioWeb.logado }">
			<li><a href="<c:url value="/produtos/novo"/>"> Novo Produto
			</a></li>
		</c:if>
		<li><a href="<c:url value="/produtos"/>">Lista Produtos</a></li>
		<li><form action="<c:url value="/produto/busca"/>" method="get">
				<input id="busca" name="nome" />
			</form></li>
	</ul>
</div>





















