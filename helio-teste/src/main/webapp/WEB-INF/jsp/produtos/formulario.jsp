<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<h3><a href="<c:url value="/produtos/menu"/>">Menu</a></h3>
<script type="text/javascript"
	src="<c:url value="/js/jquery-2.1.1.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery.validate.min.js"/>"></script>

<div id="erros">
	<ul>
		<c:forEach items="${errors}" var="error">
			<li>${error.message }</li>
		</c:forEach>
	</ul>
</div>

<form action="<c:url value="/produtos"/>" method="post">
	<fieldset>
		<legend>Adicionar Produto</legend>
		<label for="nome">Nome:</label> <input id="nome" type="text"
			name="produto.nome" value="${produto.nome }" /> <label
			for="descricao">Descri��o:</label>
		<textarea id="descricao" name="produto.descricao">
${produto.descricao }
</textarea>
		<label for="preco">Pre�o:</label> <input id="preco" type="text"
			name="produto.preco" value="${produto.preco }" />
		<button type="submit">Enviar</button>
	</fieldset>

</form>


